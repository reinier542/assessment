<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use App\Models\ScheduledMaintenanceJob;
use Illuminate\View\View;

class ScheduledMaintenanceJobController extends Controller
{
    /**
     * Display all the scheduledMaintenenceJobs.
     */
    public function get(): View
    {
        $scheduledMaintenanceJobs = ScheduledMaintenanceJob::all();
        return view('scheduledMaintenanceJobs', (['scheduledMaintenanceJobs' => $scheduledMaintenanceJobs]));
    }
}