<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use App\Models\ScheduledMaintenanceJob;
use Illuminate\View\View;
use Carbon\Carbon;


class OfferController extends Controller
{
    /**
     * Generates an offer object base on de ScheduledMaintenanceJob.
     */
    public function generate(string $id): View
    {
        $scheduledMaintenanceJob = ScheduledMaintenanceJob::find($id);

        $diffInHours = $scheduledMaintenanceJob->Timeslot->diffInHours();
        $weekendRate = $scheduledMaintenanceJob->Timeslot->calcWeekendRate();

        $offer = new Offer();
        $offer->scheduled_maintenance_job_id = $scheduledMaintenanceJob->id;
        $offer->hours = $diffInHours;
        $offer->weekend = $weekendRate;

        $offer->calcLabourCosts();

        //pre saving the offer to reserve the id
        $offer->save();

        $offer->material_costs = $offer->calcMaterialCosts($scheduledMaintenanceJob);
        $offer->price_ex_vat = $offer->labour_costs + $offer->material_costs;
        $offer->calcPriceIncVat();

        //saving the offer
        $offer->save();

        return view('offer', (['offer' => $offer]));
    }
}