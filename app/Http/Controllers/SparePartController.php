<?php

namespace App\Http\Controllers;

use App\Models\SparePart;
use Illuminate\View\View;

class SparePartController extends Controller
{
    /**
     * Display all the spare parts.
     */
    public function get(): View
    {
        $spareParts = SparePart::all();
        return view('spareParts', (['spareParts' => $spareParts]));
    }

}