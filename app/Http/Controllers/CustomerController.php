<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\View\View;

class CustomerController extends Controller
{
    /**
     * Display all the customers.
     */
    public function get(): View
    {
        $customers = Customer::all();
        return view('customers', (['customers' => $customers]));
    }

}