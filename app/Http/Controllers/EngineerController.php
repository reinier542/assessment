<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class EngineerController extends Controller
{
    /**
     * Display all the engineers.
     */
    public function get(): View
    {
        return view('engineers');
    }

}