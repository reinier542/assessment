<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\View\View;

class CarController extends Controller
{
    /**
     * Display all the cars.
     */
    public function get(): View
    {
        $cars = Car::all();
        return view('cars', (['cars' => $cars]));
    }

}