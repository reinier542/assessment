<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class MaintenanceJob extends Model
{
    use HasFactory;

    public function spareParts(): BelongsToMany
    {
        return $this->belongsToMany(SparePart::class);
    }
}
