<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use SebastianBergmann\Type\TrueType;

class Timeslot extends Model
{
    use HasFactory;

    //calculates difference of from and till in hours
    public function diffInHours() 
    {
        $timeslotFrom = new Carbon($this->from);

        $diff = $timeslotFrom->diffInHours(new Carbon($this->till));

        return $diff;
    }

    //checks if its weekend
    public function calcWeekendRate()
    {
        $from = new Carbon($this->from);
        $till = new Carbon($this->till);

        if($from->isWeekend() || $till->isWeekend())
        {
            return true;
        }

        return false;
    }
}
