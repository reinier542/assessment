<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class SparePart extends Model
{
    use HasFactory;


    public function models(): BelongsToMany
    {
        return $this->belongsToMany(CarModel::class);
    }

    public function scheduledMaintenanceJobs(): BelongsToMany
    {
        return $this->belongsToMany(SchededuledMaintenanceJob::class);
    }   
}
