<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ScheduledMaintenanceJob extends Model
{
    use HasFactory;

    public function MaintenanceJobs(): HasMany
    {
        return $this->hasMany(MaintenanceJob::class);
    }

    public function Car(): BelongsTo
    {
        return $this->belongsTo(Car::class);
    }

    public function Timeslot(): BelongsTo
    {
        return $this->belongsTo(Timeslot::class);
    }
}
