<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Arr;

class Offer extends Model
{
    use HasFactory;

    //calculates labour costs
    public function calcLabourCosts()
    {
        if(!$this->weekend) {
            $this->labour_costs = $this->hours * 80;
        } else {
            $this->labour_costs = ($this->hours * 80) * 1.5;
        }
    }

    //stores the material costs rows in the database and returns total price
    public function calcMaterialCosts($scheduledMaintenanceJob)
    {
        $offerMaterialArray = [];
        $price = 0;
        foreach($scheduledMaintenanceJob->MaintenanceJobs as $maintenanceJobs){
            
            foreach($maintenanceJobs->spareParts as $sparePart) {
                $offerMaterial = new OfferMaterial();
                $offerMaterial->offer_id = $this->id;
                $offerMaterial->part = $sparePart->name;
                $offerMaterial->maintenance = $maintenanceJobs->maintenance;
                $offerMaterial->price = $sparePart->price;

                array_push($offerMaterialArray, $offerMaterial);

                $price = $price + $sparePart->price;
            }

        }

        $this->materialParts()->saveMany($offerMaterialArray);

        return $price;
    }

    //calculates price inc VAT
    public function calcPriceIncVat() {
        $this->price_inc_vat = round($this->price_ex_vat + (($this->price_ex_vat / 100) * 21), 2);
    }

    public function ScheduledMaintenanceJob(): BelongsTo
    {
        return $this->belongsTo(ScheduledMaintenanceJob::class);

    }

    public function materialParts(): HasMany
    {
        return $this->hasMany(OfferMaterial::class);
    }
}
