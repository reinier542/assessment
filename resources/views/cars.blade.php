<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Cars') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">


                <table class="table w-full">
                    <thead>
                        <tr>
                            <th class="text-left">Model</th>
                            <th class="text-left">License plate</th>
                            <th class="text-left">Owner</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($cars as $car)
                        <tr>
                            <td>{{ $car->CarModel->carBrandAndModel() }}</td>
                            <td>{{ $car->license_plate }}</td>
                            <td>{{ $car->Customer->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
