<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Offer') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    Customer: {{ $offer->ScheduledMaintenanceJob->Car->Customer->name }}<br>
                    Car:{{ $offer->ScheduledMaintenanceJob->Car->CarModel->carBrandAndModel() }} {{ $offer->ScheduledMaintenanceJob->Car->license_plate }}<br>
                    <br>
                    <table class="table w-full">
                    <tbody>
                        <tr>
                            <td class="font-bold">Labour</td>
                            <td class="font-bold">{{ $offer->hours }} hours {{ $offer->weekend === true ? '(weekend rate)' : '' }}</td>
                            <td class="font-bold">€ {{ $offer->labour_costs }}</td>
                        </tr>
                        <tr>
                            <td class="font-bold">Material</td>
                        </tr>
                    @foreach($offer->materialParts as $materialPart)
                        <tr>
                            <td>{{ $materialPart->part }}</td>
                            <td>{{ $materialPart->maintenance }}</td>
                            <td>€ {{ $materialPart->price }}</td>
                        </tr>
                    @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="font-bold">€ {{ $offer->material_costs }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="font-bold">ex. VAT</td>
                            <td class="font-bold">€ {{ $offer->price_ex_vat }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="font-bold">inc. VAT</td>
                            <td class="font-bold">€ {{ $offer->price_inc_vat }}</td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
