<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Spare parts') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">


                <table class="table w-full">
                    <thead>
                        <tr>
                            <th class="text-left">Timeslot</th>
                            <th class="text-left">Customer</th>
                            <th class="text-left">Car</th>
                            <th class="text-left">Description</th>
                            <th class="text-left">Maintenance</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($scheduledMaintenanceJobs as $scheduledMaintenanceJob)
                        <tr>
                            <td>{{ $scheduledMaintenanceJob->Timeslot->from }} - <br>{{ $scheduledMaintenanceJob->Timeslot->till }}</td>
                            <td>{{ $scheduledMaintenanceJob->Car->Customer->name }}</td>
                            <td>{{ $scheduledMaintenanceJob->Car->license_plate }}</td>
                            <td>{{ $scheduledMaintenanceJob->description }}</td>
                            <td>
                                <ul>
                                    @foreach ($scheduledMaintenanceJob->MaintenanceJobs as $maintenanceJob  )
                                        <li>{{ $maintenanceJob->maintenance }}</li>
                                        <ol>
                                            @foreach ($maintenanceJob->spareParts as $sparePart)
                                                <li>- {{ $sparePart->name }}</li>
                                            @endforeach
                                        </ol> 
                                    @endforeach
                                </ul>
                            </td>
                            <td><a href="/offer/{{ $scheduledMaintenanceJob->id }}" >Generate offer</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
