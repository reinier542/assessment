<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('maintenance_job_spare_part', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('maintenance_job_id');
            $table->unsignedBigInteger('spare_part_id');

            $table->foreign('maintenance_job_id')->references('id')->on('maintenance_jobs');
            $table->foreign('spare_part_id')->references('id')->on('spare_parts');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('maintenance_job_spare_part');
    }
};
