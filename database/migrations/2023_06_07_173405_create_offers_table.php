<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->integer('hours');
            $table->boolean('weekend');
            $table->decimal('labour_costs');
            $table->decimal('material_costs')->nullable();
            $table->decimal('price_ex_vat')->nullable();
            $table->decimal('price_inc_vat')->nullable();

            $table->unsignedBigInteger('scheduled_maintenance_job_id');

            $table->foreign('scheduled_maintenance_job_id')->references('id')->on('scheduled_maintenance_jobs');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offers');
    }
};
