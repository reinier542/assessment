<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('scheduled_maintenance_jobs', function (Blueprint $table) {
            $table->id();
            $table->text('description');

            $table->unsignedBigInteger('car_id');
            $table->unsignedBigInteger('timeslot_id');

            $table->foreign('car_id')->references('id')->on('cars');
            $table->foreign('timeslot_id')->references('id')->on('timeslots');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('scheduled_maintenance_jobs');
    }
};
