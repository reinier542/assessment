<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SparePartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Brake disks
        DB::table('spare_parts')->insert([
            'name' => 'Brake disk',
            'price' => 50.99
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 1,
            'car_model_id' => 1
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 1,
            'car_model_id' => 3
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 1,
            'car_model_id' => 5
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 1,
            'car_model_id' => 7
        ]);

        //windshield wipers
        DB::table('spare_parts')->insert([
            'name' => 'Windshield wipers',
            'price' => 9.99
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 2,
            'car_model_id' => 1
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 2,
            'car_model_id' => 2
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 2,
            'car_model_id' => 3
        ]);

        //cooling fluid
        DB::table('spare_parts')->insert([
            'name' => 'Cooling fluid',
            'price' => 7
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 3,
            'car_model_id' => 1
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 3,
            'car_model_id' => 3
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 3,
            'car_model_id' => 4
        ]);

        DB::table('car_model_spare_part')->insert([
            'spare_part_id' => 3,
            'car_model_id' => 5
        ]);
    }
}
