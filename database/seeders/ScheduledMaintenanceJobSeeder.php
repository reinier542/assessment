<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScheduledMaintenanceJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('timeslots')->insert([
            'from' => Carbon::parse('2023-06-08 09:00:00'),
            'till' => Carbon::parse('2023-06-08 11:00:00')
        ]);

        DB::table('scheduled_maintenance_jobs')->insert([
            'description' => 'Customer is in a hurry',
            'car_id' => 1,
            'timeslot_id' => 1
        ]);

        DB::table('timeslots')->insert([
            'from' => Carbon::parse('2023-06-10 09:00:00'),
            'till' => Carbon::parse('2023-06-10 11:00:00')
        ]);

        DB::table('scheduled_maintenance_jobs')->insert([
            'description' => 'Customer is in a hurry',
            'car_id' => 1,
            'timeslot_id' => 2
        ]);
    }
}
