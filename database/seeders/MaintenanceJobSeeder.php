<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaintenanceJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('maintenance_jobs')->insert([
            'maintenance' => 'Replace',
            'scheduled_maintenance_job_id' => 1
        ]);

        DB::table('maintenance_job_spare_part')->insert([
            'spare_part_id' => 1,
            'maintenance_job_id' => 1
        ]);

        DB::table('maintenance_jobs')->insert([
            'maintenance' => 'Clean',
            'scheduled_maintenance_job_id' => 1
        ]);

        DB::table('maintenance_job_spare_part')->insert([
            'spare_part_id' => 2,
            'maintenance_job_id' => 2
        ]);

        DB::table('maintenance_jobs')->insert([
            'maintenance' => 'Restore',
            'scheduled_maintenance_job_id' => 1
        ]);

        DB::table('maintenance_job_spare_part')->insert([
            'spare_part_id' => 3,
            'maintenance_job_id' => 3
        ]);

        DB::table('maintenance_jobs')->insert([
            'maintenance' => 'Replace',
            'scheduled_maintenance_job_id' => 2
        ]);

        DB::table('maintenance_job_spare_part')->insert([
            'spare_part_id' => 1,
            'maintenance_job_id' => 4
        ]);

        DB::table('maintenance_jobs')->insert([
            'maintenance' => 'Clean',
            'scheduled_maintenance_job_id' => 2
        ]);

        DB::table('maintenance_job_spare_part')->insert([
            'spare_part_id' => 2,
            'maintenance_job_id' => 5
        ]);

        

        
    }
}
