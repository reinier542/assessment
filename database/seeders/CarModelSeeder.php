<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Kia
        DB::table('car_models')->insert([
            'name' => 'Picanto',
            'car_brand_id' => 1
        ]);

        DB::table('car_models')->insert([
            'name' => 'Ceed',
            'car_brand_id' => 1
        ]);

        //Ford
        DB::table('car_models')->insert([
            'name' => 'Focus',
            'car_brand_id' => 2
        ]);

        DB::table('car_models')->insert([
            'name' => 'Fiesta',
            'car_brand_id' => 2
        ]);

        //Mazda
        DB::table('car_models')->insert([
            'name' => 'MX-5',
            'car_brand_id' => 3
        ]);

        DB::table('car_models')->insert([
            'name' => '3',
            'car_brand_id' => 3
        ]);

        //Ope;
        DB::table('car_models')->insert([
            'name' => 'Corsa',
            'car_brand_id' => 4
        ]);

        DB::table('car_models')->insert([
            'name' => 'Astra',
            'car_brand_id' => 4
        ]);
    }
}
