<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('car_brands')->insert([
            'name' => 'Kia'
        ]);

        DB::table('car_brands')->insert([
            'name' => 'Ford'
        ]);

        DB::table('car_brands')->insert([
            'name' => 'mazda'
        ]);

        DB::table('car_brands')->insert([
            'name' => 'Opel'
        ]);
    }
}
