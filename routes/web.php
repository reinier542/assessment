<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EngineerController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SparePartController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\ScheduledMaintenanceJobController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/engineers', [EngineerController::class, 'get'])->name('engineers.get');
    Route::get('/customers', [CustomerController::class, 'get'])->name('customers.get');
    Route::get('/spare-parts', [SparePartController::class, 'get'])->name('spareParts.get');
    Route::get('/cars', [CarController::class, 'get'])->name('cars.get');
    Route::get('/scheduled-maintenance-jobs', [ScheduledMaintenanceJobController::class, 'get'])->name('scheduledMaintenanceJobs.get');
    Route::get('/offer/{id}', [OfferController::class, 'generate'])->name('Offer.generate');

});

require __DIR__.'/auth.php';
